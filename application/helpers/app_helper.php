<?php 

function uang( $var, $tipe=null, $dec="0" )
{
    if ( empty($var) ) return 0;
    return 'Rp. ' . number_format(str_replace(',','.',$var), $dec,',','.').($dec=="0"?($tipe == true ? ',-' : ",00" ):'');
}

function uang2( $var, $dec="2" )
{
    if ( empty($var) ) return 0;
    if ( !is_numeric($var)) return $var;
    if(is_numeric( $var ) && floor( $var ) != $var){
            return number_format(str_replace(',','.',$var),$dec,',','.');
    }else{   
            return number_format(str_replace(',','.',$var),0,',','.');
    }
}

function bulan($bulan)
{
    $aBulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

    return $aBulan[$bulan];
}

function hari($hari)
{
    $data  = date('N', strtotime($hari));
    $aHari = ['', 'Senin', 'Selasa','Rabu','Kamis',"Jum'at",'Sabtu','Minggu'];

    return $aHari[$data];
}

function tgl_format($tgl , $format = "")
{
    if ($format == "") {
        $tanggal    = date('d', strtotime($tgl));
        $bulan      = bulan( date('n', strtotime($tgl))-1 );
        $tahun      = date('Y', strtotime($tgl));
        return $tanggal.' '.$bulan.' '.$tahun;
    }elseif ($format == 'd/m/Y') {
        $tanggal    = date('d', strtotime($tgl));
        $bulan      = date('m',strtotime($tgl));
        $tahun      = date('Y', strtotime($tgl));
        return $tanggal.'/'.$bulan.'/'.$tahun;
    }

}

function date_format_indo($tgl,$style=null)
{
    $exp = explode(' ', $tgl);
    $detik      = date('s', strtotime($tgl));
    $menit      = date('i', strtotime($tgl));
    $jam        = date('H', strtotime($tgl));
    $tanggal    = date('d', strtotime($tgl));
    $bulan      = bulan( date('n', strtotime($tgl))-1 );
    $tahun      = date('Y', strtotime($tgl));

    if( empty($exp[1]) ){
        return $tanggal.' '.$bulan.' '.$tahun;
    }else{
        if($style == true){
            return $tanggal.' '.$bulan.' '.$tahun.' '.$jam.':'.$menit.':'.$detik;
        }else{
            return $tanggal.' '.$bulan.' '.$tahun;
        }
    }
}

function bulan_tahun($param){
    $bulan = bulan( date('n', strtotime($param))-1 );
    $tahun = date('Y', strtotime($param));
    return $bulan.' '.$tahun;
}

function date_value($tgl)
{
   $date        = date_format_indo($tgl);
   $date_indo   = explode(' ', $date);

   $hr  = hari($tgl);
   $tg  = Terbilang($date_indo[0]);
   $bln = $date_indo[1];
   $thn = Terbilang($date_indo[2]);

   return $hr . ' tanggal ' . $tg . ' bulan ' . $bln . ' tahun ' . $thn;
}

function bln_thn($tgl, $deli)
{
    $data   = explode($deli, $tgl);
    $x      = (intval($data[0])-1);

    return bulan($x) . ' ' . $data[1];
}

function tgl_edit($tgl)
{
    $parts = explode('-', $tgl);
    $date  = "$parts[2]/$parts[1]/$parts[0]";
    
    return $date;
}

 ?>