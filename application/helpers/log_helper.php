<?php 

    function helper_log($tipe = "", $str = ""){
        $CI =& get_instance();
     
        if (strtolower($tipe) == "login"){
            $log_tipe   = 0;
        }
        elseif(strtolower($tipe) == "logout")
        {
            $log_tipe   = 1;
        }
        elseif(strtolower($tipe) == "add"){
            $log_tipe   = 2;
        }
        elseif(strtolower($tipe) == "edit"){
            $log_tipe  = 3;
        }
        else{
            $log_tipe  = 4;
        }
     
        // paramter
        $username               = $CI->session->userdata('username');
       
     
        $ip = $_SERVER['REMOTE_ADDR'];
        $tanggal2 = date('Y-m-d H:i:s');
        $tanggal = date('Y-m-d');
        $data = $tanggal2." - ".$username." - ".$tipe." - ".$str." - ".$ip." - ".current_url()."\r\n";

        if (! file_put_contents("./application/logs/log_$tanggal.txt", $data, FILE_APPEND)===true) {
            write_file("./application/logs/log_$tanggal.txt", $data);
        }
     
    }

?>