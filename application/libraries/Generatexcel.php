<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generatexcel
{
	protected $ci;

	public function __construct()
	{
        $this->ci =& get_instance();
	}

	public function generate($data_array = array()){
		// We'll be outputting an excel file
		header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

		// It will be called file.xls
		header('Content-Disposition: attachment; filename="file.xlsx"');
		/** Include path **/
		ini_set('include_path', ini_get('include_path').';../Classes/');

		/** PHPExcel */
		include 'PHPExcel.php';

		/** PHPExcel_Writer_Excel2007 */
		include 'PHPExcel/Writer/Excel2007.php';

		// Create new PHPExcel object
		// echo date('H:i:s') . " Create new PHPExcel object\n";
		$objPHPExcel = new PHPExcel();
		// echo date('H:i:s') . " Add some data\n";
		$objPHPExcel->setActiveSheetIndex(0);

		$row = 1;
		foreach ($data_array as $key => $data) {
			
			$row++;
		}

		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Hello');
		$objPHPExcel->getActiveSheet()->SetCellValue('B2', 'world!');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Hello');
		$objPHPExcel->getActiveSheet()->SetCellValue('D2', 'world!');

		// Rename sheet
		// echo date('H:i:s') . " Rename sheet\n";
		$objPHPExcel->getActiveSheet()->setTitle('Simple');

				
		// Save Excel 2007 file
		// echo date('H:i:s') . " Write to Excel2007 format\n";
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		// Write file to the browser
		$objWriter->save('php://output');
		// $objWriter->save(str_replace('.php', '.xlsx', __FILE__));

	}

}

/* End of file Generatexcel.php */
/* Location: ./application/libraries/Generatexcel.php */
