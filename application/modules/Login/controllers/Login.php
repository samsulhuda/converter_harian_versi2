<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('alert_helper');

	}

	public function index()
	{
		if ($this->session->userdata('username')) {
			redirect('home','refresh');
		}

		$this->load->view('login');
	}

	public function act_login()
	{
		if ($this->session->userdata('username')) {

			redirect('home','refresh');
			
		}

		$this->form_validation->set_rules('username','username','required|callback_cekUsername');
		$this->form_validation->set_rules('password','Password','required');

		if ($this->form_validation->run()==false) {

			$this->session->set_flashdata('alert_msg',err_msg('Login ke aplikasi,username mungkin belum terdaftar'));
			redirect(site_url(),'refresh');

		}else{

				$cek = $this->cekPassword($this->input->post('username'),$this->input->post('password'));

				if ($cek) {

					$pilih['username'] = $this->input->post('username');
					$data = $this->app_model->get('users',NULL,$pilih)[0];

					$newdata = array(
			        'username' => $data->username,
			        'level' => $data->level,
			        'user_id' => $data->user_id,
			        'logged_in' => TRUE,
					);
			
					//set up session data
					$this->session->set_userdata($newdata);
					helper_log("login",null);
					redirect('home','refresh');

					
				}else{
					$this->session->set_flashdata('alert_msg',err_msg('Login ke aplikasi,Password tidak sesuai'));
					redirect(site_url(),'refresh');
				}
			
			}
		
		
	}

	public function cekUsername($username)
	{
		$pilih['username'] = $username;
		$validate = $this->app_model->validation('users',$pilih );
	
			if ($validate!=1) {
				return TRUE;
			}else{
				return FALSE;
			}
	}

	public function cekPassword($username,$password)
	{
		
		$pilih['username'] = $username;
		$pass_db = $this->app_model->get('users',NULL,$pilih)[0]->password;
	
		if (password_verify($password,$pass_db)) {
				
			return TRUE;
		}
			
			return FALSE;	
	
	}

	public function logout() {
		
		helper_log("logout",null);

		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect(site_url());
	}



}

/* End of file Login.php */
/* Location: ./application/modules/login/controllers/Login.php */