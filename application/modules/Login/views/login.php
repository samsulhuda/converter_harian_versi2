
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url().'/asset/' ?>/favicon.ico">

    <title>Converter Penyusutan</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url().'/asset/' ?>bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url().'/asset/' ?>signin.css" rel="stylesheet">
  </head>

  <body class="text-center">
    <form class="form-signin" action="<?php echo base_url('login/act_login') ?>" method="POST">
      <img class="mb-4" src="<?php echo base_url().'/asset/' ?>logo.jpg" alt="" height="40">
      <h1 class="h3 mb-12 font-weight-normal">Converter Penyusutan</h1><br>
      <label for="inputEmail" class="sr-only">Username</label>
      <input type="text" id="username" name="username" class="form-control" placeholder="Username" required autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
      
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      <p class="mt-5 mb-3 text-muted">&copy; 2018</p>
    </form>
  </body>
</html>
