<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pool extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('alert_helper');
		if ($this->session->userdata('logged_in')!=TRUE) {
				redirect(site_url());
			}

		$this->load->library('excel');

		$this->load->model($this->model_name,'model');
	}

	private $model_name = 'M_pool';
	private $url 		= 'Pool';
	private $prefix		= 'pool_';

	public function index()
	{	
		$data['back_url'] = base_url('pool');
		$data['url'] = 'pool';
		$data['data']   = $this->model->get_data();

		$data['url'] = 'pool';
		$this->template->view('v_home',$data);
	}

	public function add(Type $var = null)
	{
	
		if($this->input->post()){

			$this->load->library('form_validation');
			if($this->model->save()){

				if ($this->input->post('add_again') != null) {
					$this->session->set_flashdata('alert_msg',succ_msg('data berhasil di tambah'));
				redirect('pool/add','refresh');
				} 
				
				$this->session->set_flashdata('alert_msg',succ_msg('data berhasil di tambah'));
				redirect('pool','refresh');
			}

		}

		
		$data['back_url'] = base_url($this->url);
		$data['tipe']	= 'Tambah';
		$data['url'] 	= $this->url;
		$this->template->view('v_form',$data);
	}

	public function edit($id)
	{   
		if ($id == null) {
			redirect('pool','refresh');
		}

		if($this->input->post()){

			$this->load->library('form_validation');

			// action save
			if($this->model->update($id)){
				$this->session->set_flashdata('alert_msg',succ_msg('data berhasil di update'));
				redirect('pool','refresh');
				}
		}

		$data['data'] = $this->model->detail($id);
		$data['back_url'] = base_url($this->url);
		$data['url'] = 'pool';
		$this->template->view('v_form',$data);
		

	}

	public function delete($id)
	{
		if ($id == null) {
			redirect('pool','refresh');
		}
			// action delete
			if($this->model->delete($id)){
				$this->session->set_flashdata('alert_msg',succ_msg('data berhasil di delete'));
				redirect('pool','refresh');
				}
	}

	public function import($value='')
	{
		
		$data['back_url'] = base_url($this->url);
		$data['tipe']	= 'Import';
		$data['url'] 	= $this->url;
		$this->template->view('v_import',$data);
	}

	public function do_upload($value='')
	{
		
			date_default_timezone_set("Asia/Jakarta");

			if ($_FILES['file']['name']==null){
				//redirect with fash data
				$this->session->set_flashdata('alert_msg',err_msg('masukan file untuk diupload'));
				redirect($this->url.'/import','refresh');
			}
		
			
		  $fileName = $_FILES['file']['name'];
		  $config['upload_path'] = './upload/'; 
		  $config['file_name'] = $fileName;
		  $config['allowed_types'] = 'xls|xlsx|csv|ods|ots';
		  $config['max_size'] = 10000;

		  $this->load->library('upload', $config);
		  $this->upload->initialize($config); 
		  
		  if (!$this->upload->do_upload('file')) {
		   $error = array('error' => $this->upload->display_errors());
		   $this->session->set_flashdata('alert_msg',err_msg('file untuk diupload, '.$this->upload->display_errors())); 
		   redirect($this->url.'/import'); 
		  } else {
		   $media = $this->upload->data();
		   $inputFileName = 'upload/'.$media['file_name'];
		   
		   try {
		    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
		    $objPHPExcel = $objReader->load($inputFileName);
		   } catch(Exception $e) {
		    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		   }

		   $sheet = $objPHPExcel->getSheet(0);
		   $highestRow = $sheet->getHighestRow();
		   $highestColumn = $sheet->getHighestColumn();

		   //validasi file excel
		   for ($row = 2; $row <= $highestRow; $row++){ 
		   		 $rowData = $sheet->rangeToArray('B' . $row . ':' . $highestColumn . $row,
		       NULL,
		       TRUE,
		       FALSE);

		   		 //validasi data kosong
		   		 if ($rowData[0][0]==null) {
		       	 $this->session->set_flashdata('alert_msg',err_msg('Import data tidak bisa di lakukan, Silahkan cek data pada baris ke-'.$row.' di file excel anda')); 
		   		 unlink('./upload/'.$fileName);
		   		 redirect($this->url.'/import');
		       }

		   }

		   for ($row = 2; $row <= $highestRow; $row++){  
		     $rowData = $sheet->rangeToArray('B' . $row . ':' . $highestColumn . $row,
		       NULL,
		       TRUE,
		       FALSE);


		     $data = array(
		     $this->prefix."nama"=> $rowData[0][0],	
		     'created_by'				=>  $this->session->userdata('user_id'),	     
		    );		  
		     
		    $this->db->insert("pool",$data);
		   }   	

		   unlink('./upload/'.$fileName);
		   $this->session->set_flashdata('alert_msg',succ_msg('import data berhasil')); 
		   redirect($this->url);
		  }  
	}


	public function export($ex=''){

        $phpExcel = new PHPExcel();

        $objPHPExcel = $phpExcel->setActiveSheetIndex(0);
        $objPHPExcel->setCellValue('A1', 'No')
        			->setCellValue('B1', 'Nama Pool');

        if ($ex=='file') {
        	$data = $this->model->get_data();
	        $row = 2;
	        $no = 1;
	        foreach ($data as $key => $value) {
	        	$objPHPExcel->setCellValue('A'.$row, $no)
	        			->setCellValue('B'.$row,$value['pool_nama'] );
	        	$row++;
	        	$no++;
	        }
        }

        header('Content-Type: application/openxmlformats-officedocument.spreadsheetml.sheet');
        if ($ex=='file') {
	        header('Content-Disposition: attachment;filename="pool_export.xlsx"');
        }else{
	        header('Content-Disposition: attachment;filename="pool_sample.xlsx"');
        }
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
        ob_end_clean();
        $objWriter->setIncludeCharts(TRUE);
        $objWriter->save('php://output');
	}

	

}

/* End of file Login.php */
/* Location: ./application/modules/login/controllers/Login.php */