<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pool extends CI_Model {

	private $table_name = 	'pool';
	private $table_id	=	'pool_id';

	private function validating($type){
		if($type == 'add'){
			$this->form_validation->set_rules('nama', 'nama', 'trim|required');

		}
		if($type == 'update'){
			$this->form_validation->set_rules('nama', 'nama', 'trim|required');
		}
		return $this->form_validation->run();
	}

	public function detail($id){

		$this->db->select('*');
		$this->db->from($this->table_name);

		$this->db->where(array($this->table_name.'.deleted'=>'N','md5('.$this->table_id.')'=> $id));
		$this->db->order_by('pool_nama', 'asc');

		return $this->db->get()->row();

	}

	public function save(){
				
		if($this->validating('add')){
			$this->db->insert($this->table_name, array(
				'pool_nama'			=>	$this->input->post('nama'),
				'created_by'				=>  $this->session->userdata('user_id'),
				
			));
			$insert_id = $this->db->insert_id();

			return $insert_id;
		}else{
			return false;
		}
	}

	public function get_data($no_pol='')
	{

		$this->db->select('*');
		$this->db->from($this->table_name);


		$this->db->where($this->table_name.'.deleted', 'N');

		if ($no_pol != null) {
			$this->db->like('units.no_pol',$no_pol );
		}

		$data = $this->db->get()->result_array();

		return $data;
	}

	public function update($id){
		if($this->validating('update')){
			$this->db->where(array('deleted'=>'N','md5('.$this->table_id.')' => $id));
			$this->db->update($this->table_name, array(
				'pool_nama'		=>	$this->input->post('nama'),
			));
			return true;
		}else{
			return false;
		}
	}

	public function delete($id){
		
		$this->db->where('md5('.$this->table_id.')', $id);
		$this->db->update($this->table_name, array(
			'deleted'		=>	'Y',
			'deleted_by'	=>  $this->session->userdata('user_id'),
		));

		return true;
		// $this->db->delete($this->table_name);
	}

	

}

/* End of file Mobilsystem.php */
/* Location: ./application/modules/mobil/models/Mobilsystem.php */