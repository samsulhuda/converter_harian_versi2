	<style type="text/css">
		a {
		    text-decoration: none !important;
		    color: black;
		}
	</style>
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><a href="<?php echo $back_url ?>"><i class="icon-arrow-left52 position-left"></a></i></a> <span class="text-semibold">Unit</span> - Detailed Unit</h4>

				<ul class="breadcrumb breadcrumb-caret position-right">
					<li><a href="<?php echo $back_url ?>">Unit</a></li>
					<!-- <li><a href="learning_detailed.html">Learning</a></li> -->
					<li class="active">Detailed</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- /page header -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

		<!-- Main sidebar -->
			<div class="sidebar sidebar-main sidebar-default sidebar-separate">
				<div class="sidebar-content">

					<!-- Sidebar search -->
					<div class="panel panel-white">
						<!-- <div class="panel-heading">
							<div class="panel-title text-semibold">
								<i class="icon-search4 text-size-base position-left"></i>
								Filter
							</div>
						</div> -->
						
						<!-- Course details -->
					<div class="sidebar-category">
						<div class="category-title">
							<span>Unit details</span>
							<ul class="icons-list">
								<li>
									<!-- <i class="icon-search4 text-size-base position-left"> -->
									<a href="#" data-action="collapse"></a>
									</i>
								</li>
							</ul>
						</div>

						<div class="category-content">
							<a href="#" class="btn bg-teal-400 btn-block content-group"><?php echo $data['0']['no_pol'] ?></a>

							<div class="form-group">
								<label class="control-label no-margin text-semibold">Merk:</label>
								<div class="pull-right"><?php echo $data['0']['merk'] ?></div>
							</div>

							<div class="form-group">
								<label class="control-label no-margin text-semibold">Tipe:</label>
								<div class="pull-right"><?php echo $data['0']['tipe'] ?></div>
							</div>

							<div class="form-group">
								<label class="control-label no-margin text-semibold">Tahun:</label>
								<div class="pull-right"><?php echo $data['0']['tahun'] ?></div>
							</div>

							<div class="form-group">
								<label class="control-label no-margin text-semibold">Pool:</label>
								<div class="pull-right"><?php echo $data['0']['pool_nama'] ?></div>
							</div>

							<div class="form-group">
								<label class="control-label no-margin text-semibold">Status:</label>
								<div class="pull-right"><span class="label label-<?php echo $data['0']['status_color'] ?>"><?php echo $data['0']['status_nama'] ?></span></div>
							</div>
							<!-- <div class="form-group">
								<label class="control-label no-margin text-semibold">Rating:</label>
								<div class="pull-right">
									<i class="icon-star-full2 text-size-base text-warning-300"></i>
									<i class="icon-star-full2 text-size-base text-warning-300"></i>
									<i class="icon-star-full2 text-size-base text-warning-300"></i>
									<i class="icon-star-full2 text-size-base text-warning-300"></i>
									<i class="icon-star-full2 text-size-base text-warning-300"></i>
								</div>
							</div> -->
						</div>
					</div>
					<!-- /course details -->
					</div>
					<!-- /sidebar search -->
				</div>
			</div>
			<!-- /main sidebar -->

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Course overview -->
				<div class="panel panel-white">
					<div class="panel-heading">
						<h6 class="panel-title text-semibold">Hitory Transaksi</h6>

						<!-- <div class="heading-elements">
							<ul class="list-inline list-inline-separate heading-text">
								<li>Rating: <span class="text-semibold">4.85</span></li>
								<li>
									<i class="icon-star-full2 text-size-base text-warning-300"></i>
									<i class="icon-star-full2 text-size-base text-warning-300"></i>
									<i class="icon-star-full2 text-size-base text-warning-300"></i>
									<i class="icon-star-full2 text-size-base text-warning-300"></i>
									<i class="icon-star-full2 text-size-base text-warning-300"></i>
									<span class="text-muted position-right">(439)</span>
								</li>
							</ul>
	                	</div> -->
					</div>
					
					<ul class="nav nav-lg nav-tabs nav-tabs-bottom nav-tabs-toolbar no-margin">
						<li class="active"><a href="#course-overview" data-toggle="tab"><i class="icon-menu7 position-left"></i> Overview</a></li>
						<!-- <li><a href="#course-attendees" data-toggle="tab"><i class="icon-people position-left"></i> Attendees</a></li>
						<li><a href="#course-schedule" data-toggle="tab"><i class="icon-calendar3 position-left"></i> Schedule</a></li> -->
					</ul>

					<div class="tab-content">
						<div class="tab-pane fade in active" id="course-overview">
							<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>No</th>
											<th>Tanggal</th>
											<th>Customer</th>
											<th>Lokasi</th>
											<th>Harga</th>
											<th>Marketing</th>
										</tr>
									</thead>
									<tbody>
											<?php $no=1; foreach ($transaksi as $key => $value): ?>
										<tr>
												<td><?php echo $no ?></td>
												<td><?php echo tgl_format($value->order_tgl) ?></td>
												<td><?php echo $value->order_customer ?></td>
												<td><?php echo $value->om_lokasi ?></td>
												<td><?php echo uang2($value->om_harga) ?></td>
												<td><?php echo $value->order_marketing ?></td>
										</tr>
											<?php $no++; endforeach ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- /course overview -->
			</div>
			<!-- /main content -->




		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->