<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><a href="<?php echo $back_url ?>"><i class="icon-arrow-left52 position-left"></a></i></a> <span class="text-semibold"><?php echo $url ?></span> - Add <?php echo $url ?></h4>

				<ul class="breadcrumb breadcrumb-caret position-right">
					<li><a href="<?php echo $back_url ?>"><?php echo $url ?></a></li>
					<!-- <li><a href="learning_detailed.html">Learning</a></li> -->
					<li class="active">Add</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- /page header -->
<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<div class="col-md-8">
					<div id="flash-messages">
				      <?php echo  $this->session->flashdata('alert_msg'); ?>
				    </div>

				<!-- Form horizontal -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Tambah data pool</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>
					<!-- <?php echo validation_errors(); ?> -->
					<div class="panel-body">
						<form class="form-horizontal" action="" method="POST">
							<fieldset class="content-group">
								<div class="form-group">
									<label class="control-label col-lg-2">Nama Pool <span class="text-danger">*</span></label>
									<div class="col-lg-10">
										<input type="text" name="nama" class="form-control" placeholder="Masukan nama pool" value="<?php echo isset($data) ? set_value("nama", $data->pool_nama) : set_value("nama"); ?>">
										<div style="color:red"><?php echo form_error('nama'); ?></div>
									</div>
								</div>
							</fieldset>

							<div class="text-right">
							<a href="<?php echo base_url($url); ?>" name="add_again" class="btn btn-default">Cancel <i class=" icon-cross3 position-right"></i></a>
								<button type="submit" class="btn btn-primary">Simpan <i class="icon-arrow-right14 position-right"></i></button>

								<?php if (!isset($data)): ?>
									<button type="submit" name="add_again" value="add_again" class="btn btn-primary">Simpan dan Tambahkan Lagi <i class="icon-windows2 position-right"></i></button>
								<?php endif ?>

							</div>
						</form>
					</div>
				</div>
				<!-- /form horizontal -->
				</div>
			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->