<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><a href="<?php echo $back_url ?>"><i class="icon-arrow-left52 position-left"></a></i></a> <span class="text-semibold"><?php echo $url ?></span> - Import <?php echo $url ?></h4>

				<ul class="breadcrumb breadcrumb-caret position-right">
					<li><a href="<?php echo $back_url ?>"><?php echo $url ?></a></li>
					<!-- <li><a href="learning_detailed.html">Learning</a></li> -->
					<li class="active">Import</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- /page header -->
<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<div class="col-md-8">
				
				<?php echo  $this->session->flashdata('alert_msg'); ?>
				 

				<!-- Form horizontal -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Import data pool</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>
					<!-- <?php echo validation_errors(); ?> -->
					<div class="panel-body">
						<form action="<?php echo base_url($url.'/do_upload') ?>" class="form-horizontal" enctype="multipart/form-data" method="post" role="form">
							<fieldset class="content-group">
								<div class="form-group">
									<label class="control-label col-lg-2">File Excel Pool<span class="text-danger">*</span></label>
									<div class="col-lg-10">
										<div class="input-group">
											<input type="file" name="file" id="file" class="file-styled-primary form-control">
	                                        
	                                       
	                                    </div>
									</div>

									<div class="col-lg-10 col-lg-offset-2">
										<div class="input-group">

											<a href="<?php echo base_url($url.'/export') ?>">
												<i class="icon-download7" style="margin-right: 12px"></i>
												download example file excel
											</a>
	                                    </div>
	                                
									</div>
								</div>
							</fieldset>

							<div class="text-right">
							<a href="<?php echo base_url($url); ?>" name="add_again" class="btn btn-default">Cancel <i class=" icon-cross3 position-right"></i></a>
								<button type="submit" class="btn btn-primary">Simpan <i class="icon-arrow-right14 position-right"></i></button>

							</div>
						</form>
					</div>
				</div>
				<!-- /form horizontal -->
				</div>
			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->