	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><a href="<?php echo $back_url ?>"><i class="icon-arrow-left52 position-left"></a></i></a> <span class="text-semibold">User</span> - List User</h4>

				<ul class="breadcrumb breadcrumb-caret position-right">
					<li><a href="<?php echo $back_url ?>">User</a></li>
					<!-- <li><a href="learning_detailed.html">Learning</a></li> -->
					<li class="active">List</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- /page header -->
<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">
			<!-- Main content -->
			<div class="content-wrapper">
					<div id="flash-messages">
                        <?php echo  $this->session->flashdata('alert_msg'); ?>
                    </div>
				<!-- Basic responsive configuration -->
				<div class="panel panel-flat col-md-6">
					
                   
					<div class="panel-heading">
						<h5 class="panel-title">List User</h5>
						
						<div class="heading-elements">
							<ul class="icons-list">
		                		
		                		<li><a href="<?php echo base_url($url.'/add') ?>" class="btn  btn-icon" data-popup="tooltip" data-original-title="Tambah Data" data-placement="top"><i class="icon-plus3"></i></a></li>
		                		<!-- <li><a href="<?php echo base_url($url.'/import') ?>" class="btn  btn-icon" data-popup="tooltip" data-original-title="Import Data" data-placement="top"><i class=" icon-download7"></i></a></li>
		                		<li><a href="<?php echo base_url($url.'/export/file') ?>" class="btn  btn-icon" data-popup="tooltip" data-original-title="Export Data Excel" data-placement="top"><i class=" icon-file-excel"></i></a></li> -->
		                	</ul>
	                	</div>
					</div>


					<table class="table datatable-responsive">
						<thead>
							<tr>
								<th width="5%">No</th>
								<th>Username</th>
								<th>Level</th>
								<th class="text-center">Actions</th>
						
								
							</tr>
						</thead>
						<tbody>
							<?php $no = 1; foreach ($data as $key => $value): ?>
							<tr>
								<td><?php echo $no; ?></td>
								
								<td><?php echo $value['username'] ?></td>
								<td><?php echo $value['level'] ?></td>
								<td class="text-center">
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<ul class="dropdown-menu dropdown-menu-right">
												<li><a href="<?php echo base_url()."user/edit/".md5($value['user_id']) ?>"><i class="icon-pencil5"></i>Edit</a></li>
												<li><a href="<?php echo base_url()."user/delete/".md5($value['user_id']) ?>" onclick="return confirm('Are you sure you want to delete this item?');"><i class="icon-file-excel"></i>Hapus</a></li>
											</ul>
										</li>
									</ul>
								</td>
							</tr>
							<?php $no++; endforeach ?>
						</tbody>
					</table>


				</div>
				<!-- /basic responsive configuration -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

