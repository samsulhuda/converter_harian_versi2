<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4>&nbsp;&nbsp; <span class="text-semibold"><?php echo $this->session->userdata('name'); ?></span> - Rubah Password</h4>

				<ul class="breadcrumb breadcrumb-caret position-right">
					<li><a href="<?php echo $back_url ?>"><?php echo $url ?></a></li>
					<!-- <li><a href="learning_detailed.html">Learning</a></li> -->
					<li class="active">Rubah Password</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- /page header -->
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<div class="col-md-8">
					<div id="flash-messages">
				      <?php echo  $this->session->flashdata('alert_msg'); ?>
				    </div>

				<!-- Form horizontal -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Ubah password</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<!-- <li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li> -->
		                	</ul>
	                	</div>
					</div>
					<!-- <?php echo validation_errors(); ?> -->
					<div class="panel-body">
						<form class="form-horizontal" action="" method="POST">
							
							
							<fieldset class="content-group">
								<div class="form-group">
									<label class="control-label col-lg-3">Password Lama<span class="text-danger">*</span></label>
									<div class="col-lg-9">
										<input type="password" name="password_lama" class="form-control"  value="<?php echo set_value("password_lama"); ?>">
										<?php if ($tipe=='edit'): ?>
										<?php endif ?>
										<div style="color:red"><?php echo form_error('password_lama'); ?></div>

									</div>
								</div>
							</fieldset>

							<fieldset class="content-group">
								<div class="form-group">
									<label class="control-label col-lg-3">Password Baru<span class="text-danger">*</span></label>
									<div class="col-lg-9">
										<input type="password" name="password" class="form-control"  value="<?php echo set_value("password"); ?>">
										<?php if ($tipe=='edit'): ?>
										<?php endif ?>
										<div style="color:red"><?php echo form_error('password'); ?></div>

									</div>
								</div>
							</fieldset>
							
							<fieldset class="content-group">
								<div class="form-group">
									<label class="control-label col-lg-3">Konfirmasi Password Baru<span class="text-danger">*</span></label>
									<div class="col-lg-9">
										<input type="password" name="confirm_password" class="form-control"  value="<?php echo set_value("confirm_password"); ?>">
										<div style="color:red"><?php echo form_error('confirm_password'); ?></div>
									</div>
								</div>
							</fieldset>
						

							<div class="text-right">
							<a href="<?php echo base_url($url); ?>" name="add_again" class="btn btn-default">Cancel <i class=" icon-cross3 position-right"></i></a>
								<button type="submit" class="btn btn-primary">Simpan <i class="icon-arrow-right14 position-right"></i></button>


							</div>
						</form>
					</div>
				</div>
				<!-- /form horizontal -->
				</div>
			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->