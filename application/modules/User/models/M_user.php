<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {

	private $table_name = 	'users';
	private $table_id	=	'user_id';

	private function validating($type){
		if($type == 'add'){
			$this->form_validation->set_rules('username', 'username', 'trim|required');
			$this->form_validation->set_rules('level', 'level', 'trim|required');
			$this->form_validation->set_rules('password', 'password', 'trim|required');
			$this->form_validation->set_rules('confirm_password', 'confirm_password', 'trim|required|matches[password]');
		}
		if($type == 'update'){
			$this->form_validation->set_rules('username', 'username', 'trim|required');
			$this->form_validation->set_rules('level', 'level', 'trim|required');
			$this->form_validation->set_rules('password', 'password', 'trim');
		}
		return $this->form_validation->run();
	}

	public function detail($id){

		$this->db->select('*');
		$this->db->from($this->table_name);

		$this->db->where(array($this->table_name.'.deleted'=>'N','md5('.$this->table_id.')'=> $id));
		$this->db->order_by('username', 'asc');

		return $this->db->get()->row();

	}

	public function save(){
				
		if($this->validating('add')){

			$password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
			$data['username'] = $this->input->post('username');
			$data['level'] = $this->input->post('level');
			$data['password'] = $password;
			$data['created_by'] = $this->session->userdata('user_id');
			$this->db->insert('users',$data);
			
			
			$insert_id = $this->db->insert_id();

			return $insert_id;
		}else{
			return false;
		}
	}

	public function get_data($id = '')
	{

		$this->db->select('*');
		$this->db->from($this->table_name);


		$this->db->where($this->table_name.'.deleted', 'N');

		if ($id != null) {
			$this->db->like('user_id',$id );
		}

		$data = $this->db->get()->result_array();

		return $data;
	}

	public function update($id){
		if($this->validating('update')){
			
			if ($this->input->post('password') != '') {
				$password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
			}else{
				$password = NULL;
			}

			$this->db->where(array('deleted'=>'N','md5('.$this->table_id.')' => $id));

				$data['username'] = $this->input->post('username');
				$data['level'] = $this->input->post('level');

			if ($password != NULL) {
				$data['password'] = $password;
			}

			$this->db->update($this->table_name, $data);

			return true;
		}else{
			return false;
		}
	}

	public function delete($id){
		
		$this->db->where('md5('.$this->table_id.')', $id);
		$this->db->update($this->table_name, array(
			'deleted'		=>	'Y',
			'deleted_by'	=>  $this->session->userdata('user_id'),
		));

		return true;
		// $this->db->delete($this->table_name);
	}

	

}

/* End of file Mobilsystem.php */
/* Location: ./application/modules/mobil/models/Mobilsystem.php */