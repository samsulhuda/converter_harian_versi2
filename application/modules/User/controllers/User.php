<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('alert_helper');

			if ($this->session->userdata('logged_in')!=TRUE) {
					redirect(site_url());
			}
			

        $this->db2 = $this->load->database('db2', TRUE);


		$this->load->model($this->model_name,'model');
	}

	private $model_name = 'M_user';
	private $url = 'User'; 

	public function index()
	{
		$data['back_url'] = base_url('user');
		$data['url'] = 'user';
		$data['data']   = $this->model->get_data();

		$data['url'] = 'user';
		$this->template->view('v_home',$data);
	}

	

	public function ubah_password($value='')
	{
		if ($this->session->userdata('status')=='') {
			redirect('User/activation','refresh');
		}

		if($this->input->post()){	

			$username = $this->session->userdata('username');
			$password = $this->input->post('password_lama');

			$sql = "SELECT * FROM tbCodeName 
					WHERE DECRYPTBYPASSPHRASE('arsipserver1@',empPass) = '$password'
					AND empCode = '$username'";
			$verify = $this->db2->query($sql)->result();
	
			if ($verify) {
				
				$this->form_validation->set_rules('password', 'password', 'trim|required');
				$this->form_validation->set_rules('confirm_password', 'confirm_password', 'trim|required|matches[password]');

				if ($this->form_validation->run()) {
					$new_pass = $this->input->post('password');


					$sql = "UPDATE tbCodeName
							SET empPass = EncryptByPassPhrase('arsipserver1@','$new_pass'),isActive = '1'
							WHERE empCode = '$username'";

					if ($this->db2->query($sql)) {
						$this->session->set_userdata('status', 1);
						$this->session->set_flashdata('alert_msg',succ_msg('berhasil melakukan perubahan password'));
						redirect('user/ubah_password','refresh');
					}else{
						$this->session->set_flashdata('alert_msg',err_msg('gagal rubah password, silahkan coba lagi'));
						redirect('user/ubah_password','refresh');
					}
					
				}else{
					$this->session->set_flashdata('alert_msg',err_msg('gagal rubah password,pasword baru dan password konfirmasi tidak cocok'));
					redirect('user/ubah_password','refresh');
				}
			}else{
			
				$this->session->set_flashdata('alert_msg',err_msg('password lama salah'));
				redirect('user/ubah_password','refresh');
			}
				exit();
		}

		// $data['data'] = $this->model->detail($id);
		$data['back_url'] = base_url($this->url);
		$data['url'] = 'user';
		$data['tipe'] = 'edit';
		$this->template->view('v_form_ubah_pass',$data);
	}

	public function activation($value='')
	{
		if ($this->session->userdata('logged_in')!=TRUE) {
				redirect(site_url());
			}else{
				if ($this->session->userdata('status')!='') {
					redirect('cv','refresh');
				}
			}

		if($this->input->post()){	

			$pilih['empCode'] = $this->session->userdata('username');
			$pilih['initPass'] = $this->input->post('password_lama');
			$verify = $this->db2->get_where('tbCodeName',$pilih)->result();
			$date = date("Y-m-d H:i:s");
	
			if ($verify) {
				
				$this->form_validation->set_rules('password', 'password', 'trim|required');
				$this->form_validation->set_rules('confirm_password', 'confirm_password', 'trim|required|matches[password]');

				if ($this->form_validation->run()) {
					$new_pass = $this->input->post('password');


					$sql = "UPDATE tbCodeName
							SET empPass = EncryptByPassPhrase('arsipserver1@','$new_pass'),isActive = '1',activationDate='$date'
							WHERE empCode = '".$pilih['empCode']."'";

					if ($this->db2->query($sql)) {
						$this->session->set_userdata('status', 1);
						$this->session->set_flashdata('alert_msg',succ_msg('berhasil melakukan perubahan password, aktivasi akun berhasil'));
						redirect('cv','refresh');
					}else{
						$this->session->set_flashdata('alert_msg',err_msg('gagal rubah password, silahkan coba lagi'));
						redirect('user/activation','refresh');
					}
					
				}else{
					$this->session->set_flashdata('alert_msg',err_msg('gagal rubah password,pasword baru dan password konfirmasi tidak cocok'));
					redirect('user/activation','refresh');
				}
			}else{
			
				$this->session->set_flashdata('alert_msg',err_msg('password lama salah'));
				redirect('user/activation','refresh');
			}
				exit();
		}

		// $data['data'] = $this->model->detail($id);
		$data['back_url'] = base_url($this->url);
		$data['url'] = 'user';
		$data['tipe'] = 'Activation';
		$this->template->view('v_form_activation',$data);
	}



}

/* End of file Login.php */
/* Location: ./application/modules/login/controllers/Login.php */