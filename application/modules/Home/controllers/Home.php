<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// $this->load->helper('alert_helper');
		// if ($this->session->userdata('logged_in')!=TRUE) {
		// 		redirect(site_url());
		// 	}

		$this->load->library('excel');

		$this->load->model($this->model_name,'model');
	}

	private $model_name = 'M_home';


	public function index()
	{	
		// $d = cal_days_in_month(CAL_GREGORIAN, 9, 2018);

		$data['back_url'] = base_url('pool');
		$data['url'] = 'home';
		$this->template->view('v_home',$data);
	}


	public function export($kode_upload='',$name_file){

		$file_name =  str_replace("%20"," ",$name_file);

        $phpExcel = new PHPExcel();

        $objPHPExcel = $phpExcel->setActiveSheetIndex(0);
        $objPHPExcel->setCellValue('A1', 'No')
        			->setCellValue('B1', 'Nopol')
        			->setCellValue('C1', 'Akun')
        			->setCellValue('D1', 'Tipe Sumber')
        			->setCellValue('E1', 'Nomor Sumber')
        			->setCellValue('F1', 'Deskripsi')
        			->setCellValue('G1', 'Nilai')
        			->setCellValue('H1', 'Umur (bulan)');

         if ($kode_upload) {
         	$objPHPExcel->setCellValue('A1', 'tgl');
         	$objPHPExcel->setCellValue('H1', '');
        	$data = $this->model->get_data();
        	if ($data==null) {
        		redirect('home','refresh');
        	}

	        $row = 2;
	        $no = 1;
	        foreach ($data as $key => $value) {
	        $objPHPExcel->setCellValue('A'.$row,PHPExcel_Shared_Date::PHPToExcel($value['tgl']) )
	        			->setCellValue('B'.$row,$value['nopol'] )
	        			->setCellValue('C'.$row,$value['akun'] )
	        			->setCellValue('D'.$row,$value['tipe_sumber'] )
	        			->setCellValue('E'.$row,$value['nomor_sumber'] )
	        			->setCellValue('F'.$row,$value['deskripsi'] )
	        			->setCellValue('G'.$row,$value['nilai'] );
	        	$row++;
	        	$no++;
	        }

	        //delete
	        $this->db->where('kode_upload', $kode_upload);
			$this->db->delete('hasil'); 
        }


        header('Content-Type: application/openxmlformats-officedocument.spreadsheetml.sheet');
       
       	if ($kode_upload) {
	    	header('Content-Disposition: attachment;filename="'.$file_name.' result2_'.$kode_upload.'.xlsx"');
	    }else{
	    	header('Content-Disposition: attachment;filename="sample_converter2_penyusutan.xlsx"');

	    }
        
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
        ob_end_clean();
        $objWriter->setIncludeCharts(TRUE);
        $objWriter->save('php://output');

        
	}

	public function do_upload($value='')

	{

			$this->db->empty_table('hasil'); 
			date_default_timezone_set("Asia/Jakarta");

			//kode_upload
			$kode_upload = rand();

			//buat tanggal
			if ($this->input->post('tgl_skip')) {
				$tgl_skip = explode(',', $this->input->post('tgl_skip'));
			}else{
				$tgl_skip = [];
			}
			
	   		$tgl = explode('-',$this->input->post('waktu'));
	   		$bln = $tgl[0];
	   		$thn = $tgl[1];
	   		

	   		// $list_tgl=[];
			// for($d=1; $d<=31; $d++)
			// {
			//     $time=mktime(12, 0, 0, $bln, $d, $thn);          
			//     if (date('m', $time)==$bln && (!in_array($d, $tgl_skip)))
			//         $list_tgl[]=date('Y-m-d', $time);  
			// }


			if ($_FILES['file']['name']==null){
				//redirect with fash data
				$this->session->set_flashdata('alert_msg',err_msg('masukan file untuk diupload'));
				redirect($this->url.'/home','refresh');
			}

			// $jumlah_hari = (count($list_tgl));
			
			
		  $fileName = $_FILES['file']['name'];

		  //for name file after download file
		  $file_name =  str_replace("%20","-",$fileName);
		  $file_name = substr($file_name, 0, strpos($file_name, "."));
		  $config['upload_path'] = './upload/'; 
		  $config['file_name'] = $fileName;
		  $config['allowed_types'] = 'xls|xlsx|csv|ods|ots';
		  $config['max_size'] = 10000;

		  $this->load->library('upload', $config);
		  $this->upload->initialize($config); 
		  
		  if (!$this->upload->do_upload('file')) {
		   $error = array('error' => $this->upload->display_errors());
		   $this->session->set_flashdata('alert_msg',err_msg('file untuk diupload, '.$this->upload->display_errors())); 
		   redirect($this->url.'/home'); 
		  } else {
		   $media = $this->upload->data();
		   $inputFileName = 'upload/'.$media['file_name'];
		   
		   try {
		    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
		    $objPHPExcel = $objReader->load($inputFileName);
		   } catch(Exception $e) {
		    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		   }

		   $sheet = $objPHPExcel->getSheet(0);
		   $highestRow = $sheet->getHighestRow();
		   $highestColumn = $sheet->getHighestColumn();

		   //validasi file excel
		   for ($row = 2; $row <= $highestRow; $row++){ 
		   		 $rowData = $sheet->rangeToArray('B' . $row . ':' . $highestColumn . $row,
		       NULL,
		       TRUE,
		       FALSE);
				
		   		 //validasi data kosong
		   		 if ($rowData[0][0]==null || $rowData[0][1]==null ) {
		       	 $this->session->set_flashdata('alert_msg',err_msg('Import data tidak bisa di lakukan, Silahkan cek data pada baris ke-'.$row.' di file excel anda.Tidak boleh ada data kosong')); 
		   		 unlink('./upload/'.$fileName);
		   		 redirect($this->url.'/home');
		       }

		   }

		   for ($row = 2; $row <= $highestRow; $row++){  
		     $rowData = $sheet->rangeToArray('B' . $row . ':' . $highestColumn . $row,
		       NULL,
		       TRUE,
		       FALSE);

		     $nilai_perbulan = $rowData[0][5]/$rowData[0][6];
		     $bulan_total = $bln+$rowData[0][6]-1;
		     $x = 31*$bulan_total;



		     $list_tgl=[];
		     for($d=1; $d<=$x; $d++)
			{
			    $time=mktime(12, 0, 0, $bln, $d, $thn);          
			    if ((date('m', $time)>=$bln) && (date('m', $time)<=$bulan_total) && (!in_array($d, $tgl_skip))){

			        $month=date('m', $time);  
			        $list_tgl[$month][]=date('Y-m-d', $time);  
			    }
			}

			 // $jumlah_hari = (count($list_tgl));

			 foreach ($list_tgl as $key => $value) {
			 	// exit();
			 	$nilai_jadi[(int)explode('-',$value[0])[1]] = $nilai_perbulan/(count($value));
			 }



		    foreach ($list_tgl as $key => $v_tgl) {
		    	$nlai = $nilai_jadi[(int)$key];
		    	foreach ($v_tgl as $key => $value) {
			    	$data[] = array(
			    		"nopol"		=> 		$rowData[0][0],	
					     "tipe_sumber"	=> 		$rowData[0][1],	
					     "nomor_sumber"	=> 		$rowData[0][2],	
					     "akun"			=> 		$rowData[0][3],	
					     "deskripsi"	=> 		$rowData[0][4],
			     		'kode_upload' 	=> 	$kode_upload,
			    		'tgl' => $value, 
			    		'nilai' =>$nlai ,
			    	);
		    	}
		    	
		    	
		    }

			 
			}   	
			
			
			$this->db->insert_batch('hasil', $data); 
		   unlink('./upload/'.$fileName);
		   $this->session->set_flashdata('alert_msg',succ_msg('import data berhasil')); 
		   redirect(base_url().'home/download/'.$kode_upload.'/'.$file_name);
		  }  
	}

	public function download($kode_upload = '',$name_file = '')
	{	
		if ($kode_upload == '') {
			redirect(base_url());
		}

		$data['kode_upload'] = $kode_upload;
		$data['name_file'] = $name_file;

		$this->template->view('v_download',$data);
	}



	

}

/* End of file Login.php */
/* Location: ./application/modules/login/controllers/Login.php */