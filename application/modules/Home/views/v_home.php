<style type="text/css">
	.formm{
		margin-bottom: 20px;
	}
</style>
	

	  <img class="mb-4 float-right" src="<?php echo base_url().'/asset/' ?>logo.jpg" alt="" height="50">
      <h1 class="mt-5">Converter Harian Versi 2</h1>
      <p class="lead">Silahkan upload file excel Anda</p>
      <p>Contoh, <a href="<?php echo base_url().'home/export' ?>">Download format excel</a></p>

      <div id="flash-messages">
        <?php echo  $this->session->flashdata('alert_msg'); ?>
    </div>

      	<form action="<?php echo base_url($url.'/do_upload') ?>" class="form-horizontal" enctype="multipart/form-data" method="post" role="form" autocomplete="off">

      		<div class="jumbotron">
				<div class="row">
					<div class="col-lg-12 formm">
						<label>file excel</label>
						<input type="file" name="file" id="file" class="form-control-file form-control">
					</div>
					<div class="col-lg-12 formm" >
						<label>bulan/tahun</label>
						<input type="text" name="waktu" class="form-control" id="datepicker">
					</div>
					<div class="col-lg-12">
						<button class="btn btn-primary float-right btn-m">Submit</button>
					</div>
				</div>



			</div>
		</form>

		<script type="text/javascript">
			$("#datepicker").datepicker( {
			    format: "mm-yyyy",
			    viewMode: "months", 
			    minViewMode: "months",
			    autoclose: true,
			    minView: 2,
			    // keepOpen: false,
			}).on('changeDate', function (ev) {
			     $(this).datepicker('hide');
			});
		</script>





