<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Converter Penyusutan</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url().'/asset/' ?>bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url().'/asset/' ?>sticky-footer.css" rel="stylesheet">
    <link href="<?php echo base_url().'/asset/datepicker/css/datepicker.css' ?>" rel="stylesheet">

    <script src="<?php echo base_url() ?>/asset/jquery.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url().'/asset/datepicker/js/bootstrap-datepicker.js' ?>"></script>
  </head>

  <body>

    <!-- Begin page content -->
    <main role="main" class="container">