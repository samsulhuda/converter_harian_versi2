
<body class="navbar-top-md-md sidebar-opposite-visible">

  <!-- Fixed navbars wrapper -->
  <div class="navbar-fixed-top">

    <!-- Main navbar -->
    <div class="navbar navbar-inverse bg-success-600">
      <div class="navbar-header">
        <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/logo_light.PNG" alt=""></a>

        <ul class="nav navbar-nav pull-right visible-xs-block">
          <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
        </ul>
      </div>

      <div class="navbar-collapse collapse" id="navbar-mobile">
      <!--   <ul class="nav navbar-nav">
          <li><a href="#">Text link</a></li>

          <li>
            <a href="#">
              <i class="icon-calendar3"></i>
              <span class="visible-xs-inline-block position-right">Icon link</span>
            </a>            
          </li>
        </ul> -->

        <ul class="nav navbar-nav navbar-right">
         <!--  <li><a href="#">Text link</a></li> -->

         <!--  <li>
            <a href="#">
              <i class="icon-cog3"></i>
              <span class="visible-xs-inline-block position-right">Icon link</span>
            </a>            
          </li> -->

          <li class="dropdown dropdown-user">
            <a class="dropdown-toggle" data-toggle="dropdown">
              <img src="assets/images/image.png" alt="">
              <span><strong><?php echo ucfirst($this->session->userdata('name')); ?></strong></span>
              <i class="caret"></i>
            </a>

            <ul class="dropdown-menu dropdown-menu-right">
              <?php if ($this->session->userdata('level')=='admin'): ?>
                <li><a href="<?php echo base_url('user') ?>"><i class="icon-user-plus"></i>Kelola user</a></li>
                <li class="divider"></li>
              <?php endif ?>
              <li><a href="<?php echo base_url('user/ubah_password') ?>"><i class="icon-cog5"></i> Ubah Password</a></li>
              <li><a href="<?php echo base_url('Login/logout') ?>"><i class="icon-switch2"></i> Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
    <!-- /main navbar -->


    <!-- Second navbar -->
    <div class="navbar navbar-default" id="navbar-second">
      <ul class="nav navbar-nav no-border visible-xs-block">
        <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-menu7"></i></a></li>
      </ul>

      <div class="navbar-collapse collapse" id="navbar-second-toggle">
        <ul class="nav navbar-nav">
          <li class="<?php echo ($url == 'Dashboard' ? 'active' : null); ?>">
            <a href="<?php echo base_url() ?>"><i class="icon-display4 position-left"></i>Home</a>
          </li>
         <!--  <li class="<?php echo ($url == 'Unit' ? 'active' : null); ?>">
            <a href="<?php echo base_url('unit') ?>"><i class="icon-align-center-horizontal"></i> Unit</a>
          </li>

          <li class="dropdown">
            <a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown">
              <i class="icon-strategy position-left"></i> Transaksi <span class="caret"></span>
            </a>

            <ul class="dropdown-menu width-200">
              <li class="dropdown-header">Transaksi</li>
              <li><a href="<?php echo base_url('transaksi') ?>"><i class="icon-align-center-horizontal"></i>Ritasi Oncall</a></li>
              <li><a href="<?php echo base_url('ritasi_dedicated') ?>"><i class="icon-align-center-horizontal"></i>Ritasi Dedicated</a></li>
              <li><a href="<?php echo base_url('kontrak') ?>"><i class="icon-user-tie"></i> Kontrak</a></li>
            </ul>
          </li> -->
          
          
        </ul>
      </div>
    </div>
    <!-- /second navbar -->

  </div>
  <!-- /fixed navbars wrapper -->
